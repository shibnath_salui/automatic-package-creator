var express = require('express');
var path = require('path');
var bodyParser = require('body-parser')

var app = express();
var archiver = require('archiver');

var multer  = require('multer');

const fs = require('fs');

var formidable = require('formidable');

app.use(express.urlencoded());
app.use(express.json());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//Setting assets path
app.use('/assets', express.static(__dirname + '/views/assets/')); 

app.get('/', ( req, res) => {
    res.render('index')
});

app.get('/explore-directory', ( req, res) => {
    
    var folderPath = req.param('folderPath');

    fs.readdir(folderPath, (err, folderContent) => {
        if( err ) {
            console.log('Exception!!!');
            res.send('Exception: Invalid folder path!!!.');
        }else {
            var folderList = [];
            folderContent.forEach( ( folder ) => {
                if( fs.lstatSync( folderPath + '\\'+ folder ).isDirectory() ) {
                    folderList.push(folder);
                }
            })

            fs.readFile( 'status.json' , function (err, data) {
                if( err ) {
                    res.render('explore-directory', { folderContent: folderList, folderPath: folderPath, disClaimerSheetExist: false });
                } else {
                    
                    var disclaimerSheetInfo = JSON.parse(data);
                    res.render('explore-directory', 
                        { 
                            folderContent: folderList, 
                            folderPath: folderPath, 
                            disClaimerSheetExist: true,
                            disclaimerSheetLocation: disclaimerSheetInfo.fileLocation,
                            disclaimerSheetName: disclaimerSheetInfo.fileInfo.fileName,
                            disclaimerSheetLastModified: disclaimerSheetInfo.fileInfo.lastModified
                        }
                    );
                }
            });

        }
    });
})

app.post('/uploadNewDisclaimerSheet', ( req, res) => {

    var source = req.headers.referer; 
    
    var form = new formidable.IncomingForm();
    fs.mkdir('./uploads/disclaimer-sheet', { recursive: true }, (err) => {
        if (err) throw err;
        
        form.uploadDir = "./uploads/disclaimer-sheet";       //set upload directory
        form.keepExtensions = true;     //keep file extension

        form.parse(req, function(err, fields, files) {
            
            //TESTING
            console.log('newDisclaimerSheetName',fields.newDisclaimerSheetName)
            
            console.log("file size: " + JSON.stringify(files.newDisclaimerSheet.size));
            console.log("file path: " + JSON.stringify(files.newDisclaimerSheet.path));
            console.log("file name: " + JSON.stringify(files.newDisclaimerSheet.name));
            console.log("file type: " + JSON.stringify(files.newDisclaimerSheet.type));
            console.log("LastModifiedDate: " + JSON.stringify(files.newDisclaimerSheet.lastModifiedDate));
            
            var fileLocation, fileName;

            if( fields.newDisclaimerSheetName === null || fields.newDisclaimerSheetName === "" ){
                //Rename the file to its original name
                console.log('Rename the file to its original name');
                
                fs.rename(files.newDisclaimerSheet.path, './uploads/disclaimer-sheet/' + files.newDisclaimerSheet.name, function(err) {
                    if (err){
                        console.log('Uploaded File rename exception!!');
                    }else {
                        fileLocation = __dirname + '\\uploads\\disclaimer-sheet\\'+ files.newDisclaimerSheet.name;
                        fileName = files.newDisclaimerSheet.name;
                        var data = {
                            "disclaimerSheetExist" : true,
                            "fileLocation": fileLocation,
                            "fileInfo": {
                                "fileName": fileName,
                                "lastModified": JSON.stringify(files.newDisclaimerSheet.lastModifiedDate)
                            }
                        }
                        
                        fs.writeFileSync('status.json', JSON.stringify(data));
                        console.log('renamed complete');
                        res.redirect(source);
                    } 
                });
            }else {
                // //Rename the file to its original name
                console.log('Rename the file to its custom name')
                fs.rename(files.newDisclaimerSheet.path, './uploads/disclaimer-sheet/'+ fields.newDisclaimerSheetName + '.xlsx', function(err) {
                    if (err){
                        console.log('Uploaded File rename exception!!');
                    }else {
                        fileLocation = __dirname + '\\uploads\\disclaimer-sheet\\'+ fields.newDisclaimerSheetName + '.xlsx';
                        fileName = fields.newDisclaimerSheetName + '.xlsx';
                        var data = {
                            "disclaimerSheetExist" : true,
                            "fileLocation": fileLocation,
                            "fileInfo": {
                                "fileName": fileName,
                                "lastModified": JSON.stringify(files.newDisclaimerSheet.lastModifiedDate)
                            }
                        }
                        
                        fs.writeFileSync('status.json', JSON.stringify(data));
                        console.log('File renamed!');
                        res.redirect(source);
                    } 
                });
            }

            
        });
    })
})

app.post('/make-zip', (req, res) => {
    
    var reqBody = JSON.parse(req.param('qry'));

    makeZip( reqBody ).then( package => {
        res.render('output', { output: package, outputPath: reqBody.outputPath })
    }).catch( err => {
        res.send( JSON.stringify( 'Exception in creating packages!<br><br>' + err ) )
    })
    
})


function makeZip(reqBody) {
    
    return new Promise( (resolve, reject) => {
        var folderPath = reqBody.folderPath + '\\';
        var outputPath = reqBody.outputPath + '\\';
        var engineFolderName = reqBody.engineFolderName;
        var packageArray = reqBody.packages;
        var disclaimerSheet = reqBody.disclaimerSheet;
        var disclaimerSheetLocation = reqBody.disclaimerSheetLocation;
        var disclaimerSheetName = reqBody.disclaimerSheetName;

        var resObj = [];
        k = 0;
        for(var i in packageArray) {
            

            if( disclaimerSheet ) {
                var dirNames = [ packageArray[i].updatedFolderName, engineFolderName, disclaimerSheetLocation ];
                console.log('***Packages with disclaimer sheet--->',disclaimerSheetLocation);
            } else {
                var dirNames = [ packageArray[i].updatedFolderName, engineFolderName ];
                console.log('***Packages without disclaimer sheet--->',disclaimerSheetLocation);
                
            }

            
            var archive = archiver.create('zip', {});
            archive.on('error', function(err){
                console.log('ZIP exception');
                reject('Zip creation exception!');
            });

            var output = fs.createWriteStream( outputPath + packageArray[i].updatedFolderName + '.zip');

            output.on('close', function() {
                console.log(archive.pointer() + ' total bytes');
                console.log('archiver has been finalized and the output file descriptor has closed.');
                //Final response
                if( k === packageArray.length - 1) {
                    resolve(resObj);
                }
                k++;
            });

            archive.pipe(output);

            dirNames.forEach(function (dirName, idx) {
                // 1st argument is the path to directory 
                // 2nd argument is how to be structured in the archive (thats what i was missing!)
                
                if( disclaimerSheet ) {
                    if( idx == dirNames.length - 1) {
                        console.log('['+idx+']' + dirName )
                        archive.file( dirName , {name: disclaimerSheetName } )
                    }else {
                        console.log('['+idx+']' + folderPath + dirName)
                        archive.directory(folderPath + dirName, dirName);
                    }
                } else {
                    archive.directory(folderPath + dirName, dirName);
                }

                
            });
            archive.finalize();
            
            if( disclaimerSheet ) {
                resObj.push('Zip created for: ' + packageArray[i].updatedFolderName + ' with engine folder (With Disclaimer sheet)');
            } else {
                resObj.push('Zip created for: ' + packageArray[i].updatedFolderName + ' with engine folder');
            }
            
            
        }
        //resolve(resObj);
    })
}

app.listen(8000, () => {
    console.log("Application is running on localhost at port 8000");
})