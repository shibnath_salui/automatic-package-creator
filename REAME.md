# Automatic Zip package creator tool

### Description:
- This tool can create Zip packages of any verified build along with engine directory automatically rather than involving manual effort for doing the same job.
- This process will reduce our maual effort, time and most important you can do job without any human mistake as because this is a GUI based application.

### Technology used:
- NodeJS with Express framework
- Templating engine: eJS
- jQuery and Bootstrap

### Installation:
To intsall and run this application, you must need to have lastest nodeJS and npm installed on your system.
Once you install node on your system, please follow the step below:

- Open a CMD and navigate to directory where you wish to install this application.
- Clone the repo(https://bitbucket.org/shibnath_salui/automatic-package-creator/src/master/REAME.md) by using the follwoing command below:
   > git clone https://shibnath_salui@bitbucket.org/shibnath_salui/automatic-package-creator.git
- Then navigate to that cloned repo by using following command:
   > cd  automatic-package-creator
- Intsall the dependecies by using the following command below:
   > npm i
- As soon as depepecies get installed and now you need to run application by using command:
   > node index.js
- Now you need to open a browser and navigate to follwoing URL below:
   >  http://localhost:8000
- Now you can see the application is running on your local machine at port number of 8000. You can get steps from the link below:
   > http://lmhosting.learningmate.com/jwy/automated_zip_creation_process_demo/zoom_0.mp4

### Bibliography
- https://nodejs.org/en/
- https://expressjs.com/
- https://www.npmjs.com/
- https://ejs.co/
- https://jquery.com/
- https://www.w3schools.com/bootstrap/
- https://www.npmjs.com/package/archiver
- https://stackoverflow.com/

#### Thank you!